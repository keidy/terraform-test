#############
# RDS subnet
#############
resource "aws_db_subnet_group" "default" {
  name       = "${var.prefix}-rds sb"
  subnet_ids = [for subnet in var.subnets : subnet.id]
  # [subnet, subnet, subnet] ... 1
  # [id, arn, ...,] ... 2
  tags = {
    Name = "Terraform RDS subnet group"
  }
}
##############
# RDS
##############
resource "aws_db_instance" "default" {
  allocated_storage = 10
  engine            = var.engine
  engine_version    = var.engine_version
  instance_class    = var.size
  db_name           = "${var.prefix}mysql"
  username          = var.username
  password          = var.password
  # parameter_group_name = "default.mysql8.0"
  # option_group_name = "default:mysql-8.0"
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.default.name
  port                   = 3306
  publicly_accessible    = var.publicly_accessible
  vpc_security_group_ids = [var.sg["WEB/rds"].id, var.sg["SSH/ADMIN"].id]
  identifier             = "${var.prefix}-rds"
}
