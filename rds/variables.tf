variable "prefix" {
  type = string
  default = "terra"
}
variable "region" {
  type = string
  default = "ap-northeast-2"
}
variable "subnets" {
  
}
variable "sg" {
  
}
variable "engine" {
  type = string
  default = "mysql"
}
variable "engine_version" {
  type = string
  default = "8.0.27"
}
variable "username" {
  type = string
  default = "root"
}
variable "password" {
  
}
variable "size" {
  
}
variable "publicly_accessible" {
  
}