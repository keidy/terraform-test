######
# VPC
######
resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = "${var.prefix}-vpc"
  }
}
####################
# internet gateway
####################
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.prefix}-igw"
  }
}
##################
# subnet
##################
resource "aws_subnet" "subnet" {
  count      = length(var.vpc_var.cidr_list)
  vpc_id     = aws_vpc.main.id
  cidr_block = element(var.vpc_var.cidr_list, count.index)

  availability_zone       = element(var.vpc_var.availability_zone, count.index)
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = {
    Name = element(var.vpc_var.subnet_name, count.index)
  }
}
####################
# route table
####################
# main으로 지정해줘야 2개가 안생김
# route_table 안에 route를 넣고 다른 부분에서 새로운 route를 생성 후 apply 시
# 생성, 삭제를 반복하게 된다 route_table과 route는 따로 설정하도록 하자
resource "aws_default_route_table" "rtb" {
  default_route_table_id = aws_vpc.main.default_route_table_id
  tags = {
    Name = "${var.prefix}-route"
  }
}
resource "aws_route" "default" {
  route_table_id = aws_default_route_table.rtb.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.gw.id
}
############################
# route table association
############################
resource "aws_route_table_association" "link" {
  count          = var.is_associate == 1 ? length(aws_subnet.subnet[*]) : 0
  subnet_id      = element(aws_subnet.subnet, count.index).id
  route_table_id = aws_default_route_table.rtb.id
}
#######################
# VPC peering
#######################
provider "aws" {
	alias = "aws-ap-northeast-2"
	region = "ap-northeast-2"
	profile = "default"
}