variable "prefix" {
  type = string
  default = "terra"
}
variable "region" {
  type = string
  default = "ap-northeast-2"
}
variable "vpc_var" {

}
variable "vpc_cidr" {
  
}
variable "map_public_ip_on_launch" {
  default = true
}
variable "is_associate" {
  default = 1
}