output "vpc_id" {
  value = aws_vpc.main.id
}
output "subnet_ids" {
  # value = [for subnet in aws_subnet.subnet : subnet.id]
  value = aws_subnet.subnet
}
output "rtb" {
  value = aws_default_route_table.rtb
}
output "vpc" {
  value = aws_vpc.main
}
output "igw" {
  value = aws_internet_gateway.gw
}