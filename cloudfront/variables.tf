variable "prefix" {
  type = string
  default = "terra"
}
variable "region" {
  type = string
  default = "ap-northeast-2"
}
variable "s3" {
  
}
variable "alias_domain" {

}
variable "elb" {
  
}
variable "cert" {
  
}
# variable "path" {
#   type = list(string)
# }
variable "types" {
  
}
variable "route_name" {
  
}
variable "cf" {
  
}