###################
# cloudfront oai
###################
resource "aws_cloudfront_origin_access_identity" "example" {
  comment = "${var.prefix} oai"
}
####################
# cloudfront
####################
resource "aws_cloudfront_distribution" "default" {
  origin {
    domain_name = var.s3.bucket_regional_domain_name
    origin_id   = var.s3.id
    origin_path = "/static"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.example.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = var.elb.dns_name
    origin_id   = var.elb.id

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1"]
    }
  }

  enabled = true
  comment = "${var.prefix} cloudfront"

  aliases = var.alias_domain
  default_cache_behavior {
    allowed_methods  = var.cf.default_cache_behavior.allowed_methods
    cached_methods   = var.cf.default_cache_behavior.cached_methods
    target_origin_id = var.elb.id
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = var.cf.default_cache_behavior.viewer_protocol_policy
    min_ttl                = var.cf.default_cache_behavior.min_ttl
    default_ttl            = var.cf.default_cache_behavior.default_ttl
    max_ttl                = var.cf.default_cache_behavior.max_ttl
  }

  ordered_cache_behavior {
    path_pattern     = var.cf.ordered_cache_behavior.path_pattern[0]
    allowed_methods  = var.cf.ordered_cache_behavior.allowed_methods
    cached_methods   = var.cf.ordered_cache_behavior.cached_methods
    target_origin_id = var.s3.id

    forwarded_values {
      query_string = false
      headers      = []

      cookies {
        forward = "none"
      }
    }

    min_ttl                = var.cf.ordered_cache_behavior.min_ttl
    default_ttl            = var.cf.ordered_cache_behavior.default_ttl
    max_ttl                = var.cf.ordered_cache_behavior.max_ttl
    compress               = true
    viewer_protocol_policy = var.cf.ordered_cache_behavior.viewer_protocol_policy[0]
  }
  ordered_cache_behavior {
    path_pattern = var.cf.ordered_cache_behavior.path_pattern[1]
    allowed_methods  = var.cf.ordered_cache_behavior.allowed_methods
    cached_methods   = var.cf.ordered_cache_behavior.cached_methods
    target_origin_id = var.elb.id
    forwarded_values {
      query_string = false
      headers      = []
      cookies {
        forward = "none"
      }
    }

    min_ttl                = var.cf.ordered_cache_behavior.min_ttl
    default_ttl            = var.cf.ordered_cache_behavior.default_ttl
    max_ttl                = var.cf.ordered_cache_behavior.max_ttl
    compress               = true
    viewer_protocol_policy = var.cf.ordered_cache_behavior.viewer_protocol_policy[1]
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn            = var.cert.arn
    minimum_protocol_version       = "TLSv1.2_2019"
    ssl_support_method             = "sni-only"
  }
}
# s3 policy 추가
data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject", "s3:GetObjectVersion"]
    resources = ["${var.s3.arn}/static/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.example.iam_arn]
    }
  }
  statement {
    actions   = ["s3:GetObject", "s3:GetObjectVersion"]
    resources = ["${var.s3.arn}/static/*"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E15R2HYGAGQW8C"]
    }
  }
}
data "aws_s3_bucket" "selected" {
  # provider = aws.aws-ap-northeast-2
  bucket = "keidy"
}
resource "aws_s3_bucket_policy" "policy" {
  bucket = data.aws_s3_bucket.selected.id
  policy = data.aws_iam_policy_document.s3_policy.json
}
##########
# Route53
##########
data "aws_route53_zone" "selected" {
  name         = var.route_name
  private_zone = false
}
resource "aws_route53_record" "terra" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.alias_domain[0]
  type    = var.types[0]
  ttl     = "300"
  records = [aws_cloudfront_distribution.default.domain_name]
}