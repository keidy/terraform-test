provider "aws" {
	alias = "aws-ap-northeast-2"
	region = "ap-northeast-2"
	profile = "default"
}
resource "aws_vpc_peering_connection" "rds" {
  peer_vpc_id = var.peer_vpc.id
  vpc_id = var.vpc.id
  peer_region = var.peer_region
}
resource "aws_vpc_peering_connection_accepter" "rds" {
  provider = aws.aws-ap-northeast-2
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
  auto_accept = true
}
resource "aws_route" "a" {
  route_table_id = var.route_table_id
  destination_cidr_block = var.peer_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
}
resource "aws_route" "b" {
  provider = aws.aws-ap-northeast-2
  route_table_id = var.peer_route_table_id
  destination_cidr_block = var.vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
}
resource "aws_route" "c" {
  provider = aws.aws-ap-northeast-2
  route_table_id = var.vpc.default_route_table_id
  destination_cidr_block = var.peer_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
}