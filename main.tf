module "lambda_vpc" {
  source = "../modules/vpc"
  vpc_var = local.vpc
  vpc_cidr = local.vpc_cidr
  prefix = local.prefix
  map_public_ip_on_launch = false
  is_associate = 0
}
resource "aws_route_table" "custom" {
  vpc_id = module.lambda_vpc.vpc_id

  tags = {
    Name = "lambda-route2"
  }
}
resource "aws_route_table_association" "public" {
  subnet_id = module.lambda_vpc.subnet_ids[0].id
  route_table_id = module.lambda_vpc.rtb.id
}
resource "aws_route_table_association" "private" {
  subnet_id = module.lambda_vpc.subnet_ids[1].id
  route_table_id = aws_route_table.custom.id
}
module "sg" {
  source = "../modules/sg"
  sg = var.sg
  vpc = module.lambda_vpc.vpc_id
  prefix = local.prefix
}
data "aws_eip" "nat" {
  id = "eipalloc-0c82253d490752f8e"
}
resource "aws_nat_gateway" "nat" {
  allocation_id = data.aws_eip.nat.id
  subnet_id = module.lambda_vpc.subnet_ids[0].id

  tags = {
    "Name" = "keidy-nat"
  }
  depends_on = [module.lambda_vpc.igw]
}
resource "aws_route" "nat" {
  route_table_id = aws_route_table.custom.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.nat.id
}
data "aws_vpc" "rds" {
  provider = aws.aws-ap-northeast-2
  filter {
    name   = "tag:Name"
    values = ["terra-vpc"]
  }
}
resource "aws_vpc_peering_connection" "rds" {
  peer_vpc_id = data.aws_vpc.rds.id
  vpc_id = module.lambda_vpc.vpc_id
  peer_region = "ap-northeast-2"
}
resource "aws_vpc_peering_connection_accepter" "rds" {
  provider = aws.aws-ap-northeast-2
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
  auto_accept = true
}
resource "aws_route" "a" {
  route_table_id = aws_route_table.custom.id
  destination_cidr_block = data.aws_vpc.rds.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
}
data "aws_route_table" "rds" {
  provider = aws.aws-ap-northeast-2
  filter {
    name   = "tag:Name"
    values = ["terra-route"]
  }
}
resource "aws_route" "b" {
  provider = aws.aws-ap-northeast-2
  route_table_id = data.aws_route_table.rds.id
  destination_cidr_block = module.lambda_vpc.vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
}