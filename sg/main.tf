resource "aws_security_group" "sg" {
  vpc_id = var.vpc

  for_each    = var.sg
  name        = each.key
  description = each.key

  dynamic "ingress" {
    for_each = each.value["port"]
    iterator = port

    content {
      from_port        = each.value["range"] ? tonumber(trim(element(split(",", port.value), 0), " ")) : port.value
      to_port          = each.value["range"] ? tonumber(trim(element(split(",", port.value), 0), " ")) : port.value
      protocol         = each.value["protocol"]
      cidr_blocks      = each.value["cidr_blocks"]
      ipv6_cidr_blocks = each.value["ipv6"] ? ["::/0"] : []
      description      = element(each.value["desc"], 0) //each.value.desc[0]
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "${var.prefix}-vpc SG"
  }
}