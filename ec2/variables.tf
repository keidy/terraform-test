variable "prefix" {
  type    = string
  default = "terra"
}
variable "region" {
  type    = string
  default = "ap-northeast-2"
}
variable "vpc_id" {

}

variable "sg" {

}
variable "subnets" {

}
variable "user_data" {
  
}
variable "key_pair" {

}
variable "lt" {
  type = map(string)
}
variable "max_size" {
  
}
variable "min_size" {
  
}
variable "desired_capacity" {
  
}
variable "test_tag" {
  
}
variable "image_version" {

}