####################
# target group
####################
resource "aws_lb_target_group" "target" {
  name     = "tf-ojt-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}
####################
# ELB
####################
resource "aws_lb" "terra" {
  name               = "${var.prefix}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [for sg in var.sg : sg.id]
  subnets            = [for subnet in var.subnets : subnet.id]

  enable_deletion_protection = false

  tags = {

  }
}
################
# lb listener
################
resource "aws_lb_listener" "server" {
  load_balancer_arn = aws_lb.terra.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target.arn
  }
}
###################
# launch template
###################
resource "aws_launch_template" "lt" {
  name = "${var.prefix}-ojt"

  iam_instance_profile {
    name = var.lt["iam_profile"]
  }

  image_id                             = var.lt["image_id"]
  instance_initiated_shutdown_behavior = "terminate"

  instance_type = var.lt.instance_type

  key_name = "keidy"

  monitoring {
    enabled = true
  }

  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [for sg in var.sg : sg.id]
  }


  user_data = filebase64(var.user_data)
}
######################
# auto scailing group
######################
resource "aws_autoscaling_group" "asg" {
  name             = "${var.prefix}-ojt-asg"
  max_size         = var.max_size
  min_size         = var.min_size
  desired_capacity = var.desired_capacity
  health_check_type = "ELB"
  # availability_zones = ["ap-northeast-2a", "ap-northeast-2b", "ap-northeast-2c"]
  vpc_zone_identifier = [for subnet in var.subnets : subnet.id]

  target_group_arns = [aws_lb_target_group.target.arn]

  launch_template {
    id      = aws_launch_template.lt.id
    version = var.lt["version"]
  }

  tag {
    key                 = "Image-Version"
    value               = var.image_version
    propagate_at_launch = true
  }
  tag {
    key                 = "ImageName"
    value               = var.test_tag
    propagate_at_launch = true
  }
  tag {
    key                 = "Name"
    value               = "${var.prefix}-ojt-asg"
    propagate_at_launch = true
  }
}